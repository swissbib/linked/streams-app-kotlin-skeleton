FROM openjdk:8
ADD . /
WORKDIR /
RUN ./gradlew -q --no-scan --no-daemon --no-build-cache distTar
RUN cd /build/distributions && tar xf app.tar

FROM openjdk:8-jre-alpine
COPY --from=0 /build/distributions/app /app
# TODO: Change name of script according to name set in settings.gradle
CMD /app/bin/streams-app-kotlin-skeleton
