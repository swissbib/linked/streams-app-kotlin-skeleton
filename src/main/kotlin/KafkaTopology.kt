/*
 * TODO: Name and short description of your application (change in Settings > Editor > Copyright > Copyright Profiles > AGPL)
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import java.util.*

class KafkaTopology(private val properties: Properties, private val log: Logger) {

    fun build(): Topology {
        val builder = StreamsBuilder()

        val source = builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
        // TODO: Your processor nodes here


        source.to(properties.getProperty("kafka.topic.sink"))

        return builder.build()
    }
}
